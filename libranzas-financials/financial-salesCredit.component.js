/**
 * Created by OswaldoM on 25/05/2017.
 */
(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlAccountCredit = "http://localhost:3045/v1/credit/";
    const urlFinancial = "http://localhost:3045/v1/financial"

    var saleCredit = function ($http,model) {
        var req = {
            method: 'POST',
            url: urlAccountCredit + "changeFinancial",
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data : {
                employed: model.employed,
                financial: model.financial,
                amount: model.amount,
                dateRequest: model.dateRequest,
                price: model.price,
                salesValue : model.salesValue,
                address : model.address
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var getCredits = function ($http) {
        var req = {
            method: 'GET',
            url: urlAccountCredit,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        });
    }

    var getFinancials = function($http){
        var req = {
            method: 'GET',
            url: urlFinancial,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http) {
        var model = this;

        model.$onInit = function () {
            model.credits = [];
            model.credit = {
                employed: "",
                financial: "",
                amount: "",
                dateRequest: "",
                price: "",
                salesValue : ""
            };

            getFinancials($http).then(function (allFinancials) {
                model.financials = allFinancials;
            });

            model.getCreditsAccount();
        }
        
        model.getCreditsAccount = function () {
            getCredits($http).then(function (res) {
                model.credits = res;
            });
        }

        model.sale = function (currentModel) {
            currentModel.financial = model.credit.financial;
            saleCredit($http, currentModel).then(function (res) {
                console.log(res);
                model.getCreditsAccount();
            });

        };
    }

    module.component("salesCredit",{
        templateUrl: "libranzas-financials/financial-salesCredit.component.html",
        controllerAs: "model",
        controller: ["$http", controller]
    });
})();