(function(){
    "use strict";

    var module = angular.module("libranzaApp");

    module.component("financialApp",{
        templateUrl : "/libranzas-financials/financial-app.component.html",
        $routeConfig : [
            { path : "/list" , component : "financialList" , name :"List"},
            { path : "/about" , component : "appAbout", name : "About"},
            { path : "/credit" , component : "creditList", name : "AccountCreditList"},
            { path : "/sales" , component : "salesCredit", name : "SalesCreditList"},
            { path : "/new" , component : "financialNew" , name :"New"},
            { path : "/**" , redirectTo : ["List"] }
        ]
    });

})();