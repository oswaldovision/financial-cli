(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/financial/"

    var fetchFinancials = function ($http) {
        var req = {
            method: 'GET',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        });
    }

    var financialUpdate = function ($http, financial) {
        var req = {
            method: 'POST',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data: financial
        }

        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var financialDelete = function ($http, nit) {
        var req = {
            method: 'DELETE',
            url: urlLibranzas + nit,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }

        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http) {
        var model = this;
        model.financials = [];
        model.financial = {
            nit: "",
            name: "",
            address: "",
            phones: "",
        };

        model.$onInit = function () {
            fetchFinancials($http).then(function (financials) {
                model.financials = financials;
            });
        }

        model.editFinancial = function (financial) {
            model.financial = financial;
        }

        model.updateFinancial = function () {
            financialUpdate($http, this.financial).then(function () {
                fetchFinancials($http).then(function (financials) {
                    model.financials = financials;
                });
                model.financial = {
                    nit: "",
                    name: "",
                    address: "",
                    phones: "",
                };
            });

        }

        model.removeFinancial = function (nit) {
            financialDelete($http, nit).then(function () {
                fetchFinancials($http).then(function (financials) {
                    model.financials = financials;
                });
            });

        }
    }

    module.component("financialList", {
        templateUrl: "libranzas-financials/financial-list.component.html",
        controllerAs: "model",
        controller: ['$http', controller]
    })

})()