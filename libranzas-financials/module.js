(function () {
    "use strict";

    var module = angular.module("libranzaApp", ["ngComponentRouter", "angular-loading-bar", "ui.utils.masks"]);

    module.value("$routerRootComponent", "financialApp");

    module.component("appAbout", {
        templateUrl : "libranzas-financials/financial-about.component.html"
    });
}());