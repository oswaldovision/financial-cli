/**
 * Created by OswaldoM on 24/05/2017.
 */
(function () {
    "use strict";
    var module = angular.module("libranzaApp");
    const urlLibranzas = "http://localhost:3045/v1/request";
    const urlAccountCredit = "http://localhost:3045/v1/credit";

    var addAccountCredit = function ($http, model) {
        var req = {
            method: 'PUT',
            url: urlAccountCredit,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data: {
                employed: model.employedId,
                financial: model.financialId,
                amount: model.ammount,
                dateRequest: model.dateRequest,
                price: model.price,
                salesvalue : model.salesValue
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var getRequestByFinancial = function ($http, nitFinancial) {
        var req = {
            method: 'GET',
            url: urlLibranzas + "?financialId="+nitFinancial,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        });
    }

    var validateCredit = function ($http, model) {
        var req = {
            method: 'POST',
            url: urlLibranzas,
            headers: {
                'Content-Type': "application/json",
                "authorization": "Basic YWRtaW46MTIzNDU2"
            },
            data : {
                "_id": model._id,
                "financialId": model.financialId,
                "employedId": model.employedId,
                "companyId": model.companyId,
                "ammount": model.ammount,
                "price": model.price,
                "validatedByFinancial": model.validatedByFinancial,
                "validatedByCompany": model.validatedByCompany,
                "dateRequest": model.dateRequest
            }
        }
        return $http(req).then(function (response) {
            return response.data;
        }).catch(function (error) {
            return error;
        });
    }

    var controller = function ($http) {
        var model = this;
        model.credits = [];
        model.request = {
            _id: "",
            dateRequest: "",
            ownerId: "",
            financialId: "",
            employedId: "",
            ammount: "",
            price: "",
            companyId: "",
            salesvalue: ""
        };

        model.getCredits = function () {
            getRequestByFinancial($http,model.request.financialId).then(function (res) {
                model.credits = res.filter(function (item) {
                    return !item.validatedByFinancial && item.validatedByCompany;
                });
            });
        }

        model.validate = function (currentModel) {
            currentModel.validatedByFinancial = true;
            validateCredit($http, currentModel).then(function (res) {
                console.log(res);
                model.getCredits();
            });

            currentModel.salesValue = "0";
            addAccountCredit($http,currentModel).then(function (res) {
                model.getCredits();
            })
        }

    }

    module.component("creditList", {
        templateUrl: "libranzas-financials/financial-creditList.component.html",
        controllerAs: "model",
        controller: ['$http', controller]
    })
})();